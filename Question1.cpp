#include<stdio.h>
#include<conio.h>

int owner_cost = 500; //owner_cost global variable

//FUNCTION PROTOTYPING
int estimated_income(int a,int b); 
int estimated_atendees(int a);
int estimated_cost(int a);
int instructions(int a,int b,int c,int d);
//END OF THE FUNCTION PROTOTYPING

int main(){
	
	int ticket_price,atendees = 0; //declaring variables
	
	printf("Enter the price(LKR): ");
	scanf("%i",&ticket_price); //ask for ticket price
	atendees=estimated_atendees(ticket_price); //push parameter through estimated_atendees to calculate atendees
	
	int income = estimated_income(ticket_price,atendees); //push parameters to income function
	int cost = estimated_cost(atendees); //push parameters to cost function
	int e_profit = income - cost; //profit equation
	
	if(atendees>0){
		
		//outputs
		printf("\nEstimated number of atendees: %i\n",atendees);
		printf("Estimated cost: %iLKR\n",cost);
		printf("Estimated income: %iLKR\n",income);
		printf("Estimated profit: %iLKR\n",e_profit);
		printf("\n\n..:::::INSTRUCTIONS:::::..\n");
		instructions(ticket_price,income,cost,e_profit); //instructions for owner by instruction function
		printf("\n\n..::::::::::::::::::::::..\n");
		
	}else{
		//outputs
		printf("\n..:::::INSTRUCTIONS:::::..\n\nEstimated atendees count is zero due to high price.\n Try to decrease the price to gain atendees.\n\n..::::::::::::::::::::::..\n");

	}
	
	
	return 0;
	
} //END OF THE MAIN FUNCTION

//START OF THE ESTIMATED_ATENDEES FUNCTION

int estimated_atendees(int a){
	
	int atendees=0; //local variable atendees
	
	//equations to calculate atendees count
	
	if(a==15){
		atendees = 120;
	}else if(a<15){
		atendees = 120 + (15-a)*4;
	}else{
		atendees = 120 - (a-15)*4;
	}
	
	estimated_cost(atendees);//pushing local variable atendees to enstimated cost function to calculate cost	
	
	return atendees; //function returns integer atandees value 
	
}//END OF THE ESTIMATED_ATENDEES FUNCTION

//START OF THE ESTIMATED_COST FUNCTION

int estimated_cost(int a){
	
	int e_cost=0; //e_cost local variable
	
	e_cost = owner_cost + a*3; //eaquation to calculate estimated cost
	
	return e_cost;//returning e_cost value
	
}//END OF THE ESTIMATED_COST FUNCTION

//START OF THE ESTIMATED_INCOME FUNCTION

int estimated_income(int a,int b){
	
	int income; //local variable income
	
	income = a*b; //income equation
	
	return income; //returning income value
	
}//END OF THE ESTIMATED_INCOME FUNCTION

//START OF THE INSTRUCTION FUNCTION

int instructions(int a,int b,int c,int d){ //Instruction function is created to give instructions to its user to make good decisions
	
	int ticket_price=a+1; //this function is going to check the previous values with +1 value for ticket price
	int atendees = 0; //declaring local variable atendees
	
	atendees=estimated_atendees(ticket_price);//pushing newly created +1 ticket price though estimated atendees function
	
	int income = estimated_income(ticket_price,atendees); //to calculate +1 income
	int cost = estimated_cost(atendees);//to calculate +1 cost
	int e_profit = income - cost;//to calculate +1 profit
	
	if(b>c){
		//instructions to every ticket prices.
		if(a==24){
			printf("Try to maintain current price. \nThis price gives you the highest profit.");
			
		}else if(e_profit>d){
			printf("\nYou can increase your profit by increasing the ticket price.\nBut atendees will reduce due to high price.");
			
		}else if(d>e_profit){
			printf("\nYou can increase your profit by decreasing the ticket price.\nAlso atendees count will increasing due to low price.");
			
		}
		
	}else{
		
		printf("Price is too low,\nYou can't make profits.\nYou must increase your ticket price.");
		
	}
}//END OF THE INSTRUCTION FUNCTION 



